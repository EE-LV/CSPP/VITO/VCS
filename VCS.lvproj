﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="CCSymbols" Type="Str">CSPP_BuildContent,CSPP_Core;CSPP_WebpubLaunchBrowser,None;VITO_PPDMAArraySize,8;VITO_SCOPE_TARGET_CLASS,PXIE_5170R__8CH_;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project is used to develop this application based on NI ActorFramework and CS++ libraries.

This is the AF/CS++ based control system for VITO experiment @ISOLDE/CERN.

Licensed under the EUPL, https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf

Please refer also to README.md.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2021  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{0085AF41-978F-4B96-90A7-5BD911BD49BA}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{009A6F48-E651-4AC5-9012-7F0C47699720}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{00DC2BEA-85E8-4273-AF29-B8971BA30B75}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_7</Property>
	<Property Name="varPersistentID:{01F0D947-9956-4DF4-A611-1A6DF6841499}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Reset</Property>
	<Property Name="varPersistentID:{029C449D-B62E-4A3A-807E-EB3059973A4E}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_4</Property>
	<Property Name="varPersistentID:{02AE65A7-0073-420D-9A50-6D37C36879B1}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_ResourceName</Property>
	<Property Name="varPersistentID:{02DF4569-B859-46CF-A3EF-9B9CB49FE564}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_PollingMode</Property>
	<Property Name="varPersistentID:{030831B8-68DA-4F6C-8934-324DB0DFD7BD}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_ErrorStatus</Property>
	<Property Name="varPersistentID:{035ACAC2-24BE-4481-8992-FF8F6BFE906E}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_0</Property>
	<Property Name="varPersistentID:{035CC454-A665-42BE-B607-FAA9F2B62A8F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_11</Property>
	<Property Name="varPersistentID:{0360DFD6-C0F8-4CDB-8C1E-2535F01DCFE3}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_1</Property>
	<Property Name="varPersistentID:{03B1C9CF-E211-44D6-B743-66F057D86ED8}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_PollingCounter</Property>
	<Property Name="varPersistentID:{03D46D50-FDAF-4366-B4B6-8758A57D005B}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_4</Property>
	<Property Name="varPersistentID:{04AEA2A6-AD77-448A-896A-F4FE87407003}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{092F5467-60FC-4B52-AE27-AAA95AEE6B86}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingInterval</Property>
	<Property Name="varPersistentID:{0952388B-E1DE-45F0-BEEA-3C0DA1493F9F}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_PollingDeltaT</Property>
	<Property Name="varPersistentID:{0A3B3085-4625-4585-BD5B-3CF7E419BD61}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{0A811DA3-2A18-4CA0-BC61-88ED3557B92C}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_ErrorStatus</Property>
	<Property Name="varPersistentID:{0A968C8E-F525-4B4C-A837-C1376774631D}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{0B7D8E66-D60B-4B9F-89FF-7F89851531E8}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_Initialized</Property>
	<Property Name="varPersistentID:{0CFE25E0-F113-4551-AD00-A1B9609E45FC}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Initialized</Property>
	<Property Name="varPersistentID:{0D1F9149-B961-40EE-BB7F-AE0F88EE4134}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Error</Property>
	<Property Name="varPersistentID:{0D3873B8-F867-46B8-8543-88B9EC57CAF0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{111252FC-B5C9-481F-851F-669DAB4E2CF9}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_PollingDeltaT</Property>
	<Property Name="varPersistentID:{112E931B-DC9A-46E9-BD29-2F2E4B29F3F9}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Initialized</Property>
	<Property Name="varPersistentID:{1145F046-9D93-4170-A442-21394E522231}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{1173B630-99E9-4A24-83BF-F3C27A29954D}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_PollingDeltaT</Property>
	<Property Name="varPersistentID:{11BFDD8F-A732-40F2-A0DD-E66B25BF47DA}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{11EBFAA6-887E-469D-B2D9-E27E387518CA}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Slope_2</Property>
	<Property Name="varPersistentID:{12791CDB-1532-46EB-A732-A4E6ABE1787D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{1296D75E-DBC1-4444-B8C4-868777B59E01}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_4</Property>
	<Property Name="varPersistentID:{12B542A5-ECA7-4D3A-B20D-0DCB9FA56701}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{1341C4FB-CFF1-4A0B-A29E-5F4773EBD4EC}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_Initialized</Property>
	<Property Name="varPersistentID:{139BFC5B-4F9F-4C39-A05D-4E64A8CE85A5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{141A53A0-8D65-40CE-B868-2618CFCFB598}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_5</Property>
	<Property Name="varPersistentID:{15045F35-64CE-432C-B2F2-FCDE0BF8B950}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{15409A2B-E0B9-4761-97FC-29B30925CC34}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{1607696B-DD29-4B12-A4A7-2AF4473F652B}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_PollingInterval</Property>
	<Property Name="varPersistentID:{1688E634-D94B-4355-9274-8BFB9C999540}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_ErrorMessage</Property>
	<Property Name="varPersistentID:{16B9739C-8404-4306-8C2C-537B373FAAB5}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_0</Property>
	<Property Name="varPersistentID:{177599F7-643B-4A18-AF3F-B181B43D96D6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActorProxy_Activate</Property>
	<Property Name="varPersistentID:{17E172AD-9DBF-4DF0-A4DE-14F177A4AA5A}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AO</Property>
	<Property Name="varPersistentID:{1A7CF9BE-8629-423C-8901-2B588FD5CADC}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingCounter</Property>
	<Property Name="varPersistentID:{1AE9207A-59B9-47F9-9043-22FBB16B6F7D}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_2</Property>
	<Property Name="varPersistentID:{1C97782C-5647-4AAD-9C80-D2FC208A3C28}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingTime</Property>
	<Property Name="varPersistentID:{1D045F89-A966-47FB-A370-373D63FF635F}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_PollingMode</Property>
	<Property Name="varPersistentID:{1FDCF5CE-7AA6-4C7B-B0F7-9514829B7702}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_3</Property>
	<Property Name="varPersistentID:{206B7E3D-FD1A-49B7-87D0-DD767673F2A4}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_5</Property>
	<Property Name="varPersistentID:{20922AA8-398D-442F-8E5E-FDFAA32E4D50}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_DataOverflow</Property>
	<Property Name="varPersistentID:{211E2DA8-1FF3-423C-8C4A-C42CEEA3F24B}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_PollingTime</Property>
	<Property Name="varPersistentID:{212D6604-C7B5-45A9-8863-1741B45A356E}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{24D76808-95B1-443B-BE38-192FA0A18F4D}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_PollingCounter</Property>
	<Property Name="varPersistentID:{24E9EB6B-E891-4106-840F-B61EA003CFC9}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{2537493A-39BA-489E-AA22-FD6713498986}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIOProxy_Activate</Property>
	<Property Name="varPersistentID:{255B452E-0CCD-457B-9365-262858D3842B}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_8</Property>
	<Property Name="varPersistentID:{25B8C2B7-E23F-4C96-AA1D-74FE26220925}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{274BA88B-426A-436E-9FA1-A8C58628637C}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_ResourceName</Property>
	<Property Name="varPersistentID:{2974F87C-5D3A-4C42-8B8F-7A62048C68FA}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingDeltaT</Property>
	<Property Name="varPersistentID:{2B024322-A4CD-4AFE-B8AE-1F3D65EFE0D2}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_7</Property>
	<Property Name="varPersistentID:{2B1CF9AE-305E-4AF4-98A5-28B95AACB9F6}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{2B6AF627-3EDF-49E2-906B-3F2CC5E37976}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{2B778E89-BA9A-43D4-8E34-71E1CEA936EE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{2BF3EC13-53BB-416E-A5FA-F360C3347673}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Error</Property>
	<Property Name="varPersistentID:{2C6922AE-8D33-45A7-A777-CD8809AC5522}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Vertical_Offset</Property>
	<Property Name="varPersistentID:{2D81A040-8F1F-4862-B8C5-BF1FE0492921}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_PollingTime</Property>
	<Property Name="varPersistentID:{310E7B56-2BAE-40B6-A108-CE4D4F4B3969}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{312F1B3B-7994-4DEE-AE64-EE963ED5FA97}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_11</Property>
	<Property Name="varPersistentID:{31A49CBB-B857-4F25-B796-3B7D0AE46C5C}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{31A4A58F-F5C6-40F7-8F4B-C362C215FA4F}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{321323B7-2D06-4901-B371-4B8A190B4D6B}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Window_MinWidth</Property>
	<Property Name="varPersistentID:{3272D0B6-9ADD-4909-919B-9D5160FE209C}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3432B02F-D94C-49F1-9B74-757FD4B74A57}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_PollingMode</Property>
	<Property Name="varPersistentID:{351C861C-6BE4-4BF2-8455-2D4BF45FD25B}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_4</Property>
	<Property Name="varPersistentID:{35EEE3CF-39C1-464F-AB8E-E98313A8229B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/BeepProxy_Activate</Property>
	<Property Name="varPersistentID:{37467BF2-E30D-47CA-90AD-BCA0427EC698}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingTime</Property>
	<Property Name="varPersistentID:{39B4EE0F-0767-44E4-9363-0D737B25856A}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_1</Property>
	<Property Name="varPersistentID:{3AB53D13-67CF-460E-9E79-19F0658133CE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{3BE2EAC3-305E-40A6-AB32-4594E4F60463}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{3C7ADFC1-2F97-4E7D-95F8-8E38C9BC8DF1}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{3D2FBC67-09AE-424A-8058-3A040458B555}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/WatchdogProxy_Activate</Property>
	<Property Name="varPersistentID:{3D9C121B-FAF7-4A86-AF8A-C2FB32FCE6EE}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_DriverRevision</Property>
	<Property Name="varPersistentID:{3E204A25-4F3D-4EEF-9A96-2341A88190BB}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Error</Property>
	<Property Name="varPersistentID:{3E6C3EAD-471B-4F2F-88AD-E7BE3977346F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_9</Property>
	<Property Name="varPersistentID:{3F2C6E5B-2067-4C7F-995A-EB28BE3BAEDD}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_SelftestResultCode</Property>
	<Property Name="varPersistentID:{3F93F37A-C5E7-492E-9BB6-5A13D31E6DBB}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncherProxy_Activate</Property>
	<Property Name="varPersistentID:{40F67543-EFC8-4BA5-B403-91D1C5E7996A}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{41907A2E-41FC-4DC9-A28C-C143408A8532}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_Error</Property>
	<Property Name="varPersistentID:{41C28A84-65A1-4627-981B-70513ACC6CA6}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_ErrorMessage</Property>
	<Property Name="varPersistentID:{440C10CE-2A25-4526-88AC-9A8BF874F78E}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/BeepProxy_Activate</Property>
	<Property Name="varPersistentID:{4426EB69-B6E3-4C8E-9DA1-D7767D1FEE11}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{45DAAC7C-49C5-459C-ABA5-CD626CAFBEBF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{4633A757-ADE1-4F9D-BAF4-5F5344C84218}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-AO_1</Property>
	<Property Name="varPersistentID:{47B57102-5E18-4521-B365-C49B92B83A2A}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{484B17D0-3512-4820-A51D-C124B590F9A8}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_9</Property>
	<Property Name="varPersistentID:{492E5944-5C69-43CF-81FE-4C9125187633}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_ErrorCode</Property>
	<Property Name="varPersistentID:{495E2C5D-A207-4529-91A0-487B73E9E102}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_PollingInterval</Property>
	<Property Name="varPersistentID:{49EFB6A9-B1C2-4DA1-BF18-3185BA183FEC}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{4A083266-8407-49FC-A03D-278F8FF961FE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{4A5E1799-1695-411A-8443-C5E1756E7353}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_TimeElapsed</Property>
	<Property Name="varPersistentID:{4D0D1FF2-5C0C-4A83-B501-131D8228359E}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Slope_3</Property>
	<Property Name="varPersistentID:{4E5D843B-077B-4D91-A5B9-2720A29CF4A6}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{4E89639D-D2E8-44F4-B6E7-172EE8F8AB7D}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_7</Property>
	<Property Name="varPersistentID:{4E9CEE8F-C6C0-4AA4-BEC5-5C13D02A623E}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI</Property>
	<Property Name="varPersistentID:{4F0765AF-3724-4E11-9822-16472B0739CD}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Input_Coupling</Property>
	<Property Name="varPersistentID:{4F3C2A0B-B047-4AB1-98CB-605DDC3B1AC3}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Vertical_Range</Property>
	<Property Name="varPersistentID:{50B763BA-4A78-48D3-99C3-C0DD0FCB5651}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{51265AF2-C476-4436-8A85-0F009155DA85}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{51F6B3C7-D988-4395-B173-CEF01D947666}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_PollingTime</Property>
	<Property Name="varPersistentID:{52C39B8E-8D2E-4DA1-B168-FCB4BE569A21}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{52CA4CA2-96E3-4EBE-8B65-77E2C2EA884C}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{53057623-B16E-4394-9FEA-C0CB9B9E1536}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_10</Property>
	<Property Name="varPersistentID:{53AE040A-758D-490F-8598-6BC391873622}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingMode</Property>
	<Property Name="varPersistentID:{53FB95E3-4DAE-4A02-8ABB-32A6A1451E3E}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{555D3633-5F12-4638-947F-A3FA9133519E}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Initialized</Property>
	<Property Name="varPersistentID:{562B320F-292D-465E-AD75-E224DE254672}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{566ABA31-145B-4C38-95F8-15E9E6C295E8}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{5A14ABCF-807E-4217-8E1A-7420C89E2C20}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_3</Property>
	<Property Name="varPersistentID:{5A1F05C9-42E2-4295-8F93-B53A49FDA06E}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{5B2D1601-4B6F-4791-ADFA-870FA7CF8059}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScopeProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5B9857C9-0C1C-4FDC-8AA0-61786EE142DD}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Initialized</Property>
	<Property Name="varPersistentID:{5BB2611A-C11E-4A20-8D16-68F7DADBB6E3}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_5</Property>
	<Property Name="varPersistentID:{5BD62969-0D0A-40DD-B061-5F4052FF084A}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5C6E81DF-CDCB-4AD2-BC5D-E896DD1D9390}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_6</Property>
	<Property Name="varPersistentID:{5DB52B4F-A0B5-43A5-A905-6D3D53E55C15}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_12</Property>
	<Property Name="varPersistentID:{5E18BB5F-6CB3-48F7-9715-72AC671A2D4F}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_WDAlarm</Property>
	<Property Name="varPersistentID:{5FEC2A33-E3F7-49FA-8EB8-273BEAE31DF4}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{60CBEA05-2C11-45DF-AD7D-994E781FB0D3}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{61EB7585-7619-4203-A8A1-C05FDB121E11}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Level_0</Property>
	<Property Name="varPersistentID:{62A2BFE7-E6DE-4ABF-9E69-6B40CAB05CB9}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{6381E801-FBBB-4EE9-BDE0-716DF6303CDD}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{652D8DB0-F815-4116-B9D3-0AD7D0A84447}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_6</Property>
	<Property Name="varPersistentID:{654FB059-ECF4-477D-AC0D-D916C7F7CC6D}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_7</Property>
	<Property Name="varPersistentID:{660E98BC-356F-423A-BEC9-2FA5A43399E3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorStatus</Property>
	<Property Name="varPersistentID:{66CA569C-EDC9-4E32-BACC-AEB1F22767B1}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Level_2</Property>
	<Property Name="varPersistentID:{690E13BD-22FD-4CB1-A817-629DDA1637EC}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_PollingMode</Property>
	<Property Name="varPersistentID:{69A8E432-AFE2-49B6-A5FA-6A980CBFDEE0}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{6A2017E9-3731-4A9A-B949-DB48B8B3EB9A}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{6AB0D464-465D-4D6C-8A9B-4187E4A4A862}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{6AB41C01-F2D3-474E-9D95-3377CFC8F2C2}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_10</Property>
	<Property Name="varPersistentID:{6C06E4A6-CC1E-42F8-B149-A988E8A6FE93}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_ErrorMessage</Property>
	<Property Name="varPersistentID:{6D5053F7-B277-4C37-AA22-7251DEABB4F9}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorCode</Property>
	<Property Name="varPersistentID:{6D66E4AB-0E69-4724-B5E0-543DD3CBF419}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_6</Property>
	<Property Name="varPersistentID:{6DA4602A-7825-4570-BBD5-73495EB746C6}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_PollingDeltaT</Property>
	<Property Name="varPersistentID:{6DFB121F-1BED-45F9-963D-4AD1C9085907}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/BeepProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{6E151C92-64E4-4389-AE1B-EADAA7C6CBD4}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{6E6E46BE-498B-46BF-83A8-939DE62A6DBE}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_ErrorMessage</Property>
	<Property Name="varPersistentID:{6E71EF74-F5D7-4260-B49D-6A6309FBE2A4}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_8</Property>
	<Property Name="varPersistentID:{6E966DB3-1E41-4527-8B20-3B4CD07FE754}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_1</Property>
	<Property Name="varPersistentID:{70F0FD87-1F31-46C6-AD18-38B1A2A72E4B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingIterations</Property>
	<Property Name="varPersistentID:{719E6145-E0E1-4655-8664-A6960EBBB757}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingMode</Property>
	<Property Name="varPersistentID:{72955534-04C2-472F-8073-4E17F7E6D25B}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{746AA93C-1546-41BD-8A4F-7F2EA0697EFD}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_PollingCounter</Property>
	<Property Name="varPersistentID:{74BE642A-B8C8-4CCA-8145-6CC6E8239423}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{74D19AE3-A101-4EBE-9A57-A9C6F02F7AF3}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{756A310D-21CA-4D92-BB55-DC4AABC30795}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{75B3E32A-918C-4A93-AFF1-FC3DF653FC6B}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_ErrorStatus</Property>
	<Property Name="varPersistentID:{773D6543-5404-44AF-9CDF-1412D2C87C0D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{784F9E83-1F33-4D40-97FC-F8CA27FB9505}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{7AA00CC9-BBAA-4F11-8C93-4A6E95D07D89}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI</Property>
	<Property Name="varPersistentID:{7CF92223-D502-40A0-907F-3F9798D9253D}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_ErrorStatus</Property>
	<Property Name="varPersistentID:{7D1F01DC-DD9B-489E-BCDB-2A937254BE9F}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_StreamingPulseData</Property>
	<Property Name="varPersistentID:{7E09AEAE-69A3-4188-968F-9176AD0C30A6}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Input_Bandwidth</Property>
	<Property Name="varPersistentID:{7F072A55-FDD8-4663-B6CA-B3951686526A}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Input_Impedance</Property>
	<Property Name="varPersistentID:{7F402E71-92FB-423A-854D-0066D06279F5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingIterations</Property>
	<Property Name="varPersistentID:{7FF6C2A6-F44B-43D3-800D-085017FAD05C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorCode</Property>
	<Property Name="varPersistentID:{805C50EF-0665-448B-9D5D-5DD7263B06C9}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{8081DC55-CDAE-4887-84BA-8691ADAE8E52}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_0</Property>
	<Property Name="varPersistentID:{808DB3EE-3357-4674-BD29-E7DC0A598394}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{80D802C7-98BD-4371-81E4-075E43CCD13F}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{81B6B05A-CB3C-458A-BDFC-F2C305746381}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_FirmwareRevision</Property>
	<Property Name="varPersistentID:{8207A06F-68CC-4FDD-B6A3-814BD0802287}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{8265EFE7-B259-436E-85CB-CCE3B871024A}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{82F99B45-E3AD-4BE3-8816-1C1A6DB01A1C}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_PollingIterations</Property>
	<Property Name="varPersistentID:{83680B71-F0C9-4E1C-AE9F-D54382BB6C88}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/BaseActor_Initialized</Property>
	<Property Name="varPersistentID:{84545CC7-45E5-497D-8312-F91E65C17C32}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Slope_0</Property>
	<Property Name="varPersistentID:{86524C09-90E6-4A59-BE0F-FBFBD9C39210}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{86D58E96-DDD8-41C8-9D7B-AE41135E1BAA}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/WatchdogProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{8965E937-B413-42BF-A2E8-6E4AC2E17E98}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{8ABE6E93-E356-4507-AA9D-DC2682B1B002}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_PollingMode</Property>
	<Property Name="varPersistentID:{8BB254CE-48CA-42A5-A3B0-BC35F7875706}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_Initialized</Property>
	<Property Name="varPersistentID:{8BDB3090-7DEB-4027-B59E-A4FBBDC908BB}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_PollingCounter</Property>
	<Property Name="varPersistentID:{8C541FAD-F8DE-49E7-A394-330DCFB26F51}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_12</Property>
	<Property Name="varPersistentID:{8C820053-47B5-433F-AACB-CF7F328907D1}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{8CA9F10F-1948-4597-B696-B0CD5ECC2E9E}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_ErrorCode</Property>
	<Property Name="varPersistentID:{8D2CD524-A0E7-44E6-870E-F79A1E65EFF1}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_7</Property>
	<Property Name="varPersistentID:{8F73FD87-29E9-4043-AD3F-D29ADC499FD0}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{8FFC77D1-8DD3-4325-AAB6-3FE19872AF9C}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Type</Property>
	<Property Name="varPersistentID:{918F06E3-49FF-483A-8D80-301ACCECDC52}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_15</Property>
	<Property Name="varPersistentID:{91B14D90-4B23-4153-8CCA-67A259C810AA}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_3</Property>
	<Property Name="varPersistentID:{929281D9-F4F5-45D0-91E7-F34FAE01F329}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{929D3B79-07D0-41EE-B94A-65E5FA3F1A8F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-AO_0</Property>
	<Property Name="varPersistentID:{93BDA013-22CA-4ED7-A594-566E8CD57381}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Error</Property>
	<Property Name="varPersistentID:{95D94945-9624-42CE-8848-5E454ADFECCE}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{9691C335-CF69-420C-9E08-BBEBDF760E11}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_2</Property>
	<Property Name="varPersistentID:{96E4D1E1-4C82-4070-B34C-6117E2723080}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Slope_1</Property>
	<Property Name="varPersistentID:{978361DB-AE73-4AFC-B9A7-1B49717AB97E}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{9849132E-3BAA-4903-BFC5-CA0C54A46DC3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Error</Property>
	<Property Name="varPersistentID:{998C8E7E-4BE9-4022-A8C3-B7AB958E10B8}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_14</Property>
	<Property Name="varPersistentID:{99D95179-E772-4BEC-9C3A-772252870110}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_PollingCounter</Property>
	<Property Name="varPersistentID:{9A95A61B-AF42-464E-9A80-133B13761D1B}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_1</Property>
	<Property Name="varPersistentID:{9AA29295-2D56-4657-93E5-F5A05FD4E74C}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Level_1</Property>
	<Property Name="varPersistentID:{9C66B27C-7D1E-4A2B-9532-DDA94A9E3FFD}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/WatchdogProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{9CB8BC64-6577-48BE-893C-2A85459B5F67}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_CDSScaling_Offset</Property>
	<Property Name="varPersistentID:{9CD47E1F-1843-4935-A26E-BE58B625BCB2}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{9D1173C8-D7EF-4BC8-AF88-6B47C9951BC6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{9D3F8C9E-4A4C-4D0E-BD73-9220474E38B1}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{A0E27DC4-98D9-462B-8611-E05A52254B42}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO</Property>
	<Property Name="varPersistentID:{A16F6DD2-A51A-41A8-A7BB-ECF1153EED62}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_FirmwareRevision</Property>
	<Property Name="varPersistentID:{A20E914C-9368-4ADD-9EA2-42E020E56B4F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A2A0AA42-96C8-4A2C-8F72-7F4F5E2BE1E2}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_CDSScaling_Gain</Property>
	<Property Name="varPersistentID:{A316CBC2-3FB4-4B3A-BCC7-0A51D29874EF}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_2</Property>
	<Property Name="varPersistentID:{A3670D67-EBE9-454C-9D08-969358395C57}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_10</Property>
	<Property Name="varPersistentID:{A3E30524-394C-43B8-9BCF-29BBD2A3AA67}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{A44347E7-F0EF-4BA0-9F0A-A6B5BB3283DC}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{A5E14ABF-8577-44C7-845D-E789B1C7043E}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A6AADF28-41B4-4DE5-BE5F-44276CB9C0DF}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Error</Property>
	<Property Name="varPersistentID:{A81F35CA-A5F6-4A63-8403-87AAD607F6F0}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorCode</Property>
	<Property Name="varPersistentID:{A82D55D9-F424-478B-93C4-7E9910EE1ACB}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_PollingIterations</Property>
	<Property Name="varPersistentID:{A91FF1DA-1904-41D5-808B-12CC8824B61E}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{A92C4C54-6F46-4624-9D64-5D666E42446A}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_ErrorStatus</Property>
	<Property Name="varPersistentID:{A9713CBF-0D51-45FD-B152-63BAE649359D}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{AAB2BDAF-4AA2-49AA-8F9A-F431B5B97D6B}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_3</Property>
	<Property Name="varPersistentID:{AC004EEA-2BD4-41A4-924D-B11CD12E3CA7}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_3</Property>
	<Property Name="varPersistentID:{AC47FA46-5445-431D-A03D-9A7694BFE0B5}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Reset</Property>
	<Property Name="varPersistentID:{AC93E044-530B-47B4-B9D7-0D24B5B143AE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/WatchdogProxy_Activate</Property>
	<Property Name="varPersistentID:{ADD3492B-9B44-47D4-909A-15DD94EB9397}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_SelftestResultCode</Property>
	<Property Name="varPersistentID:{ADFD2328-6B73-4427-8FE5-21291F7D2451}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_0</Property>
	<Property Name="varPersistentID:{B0165CA8-2961-4112-828B-7D88EF994407}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_SampleClockRate</Property>
	<Property Name="varPersistentID:{B0643911-5E08-40EC-BC39-FE346229A27D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorStatus</Property>
	<Property Name="varPersistentID:{B07A703F-D3CB-4FFC-9E24-146CB243E529}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_PollingTime</Property>
	<Property Name="varPersistentID:{B179AFFC-606D-404A-B9C7-1B7794105BBF}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_StreamingPulseProperties</Property>
	<Property Name="varPersistentID:{B2857177-2F76-47BD-9F11-81BE3D35D206}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_PollingIterations</Property>
	<Property Name="varPersistentID:{B5732733-4B92-45E5-ABC3-72AB6D287E03}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DriverRevision</Property>
	<Property Name="varPersistentID:{B66101C3-68F0-4B60-A1E7-9FDD47F53B1F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_11</Property>
	<Property Name="varPersistentID:{B960430D-BBF6-493F-913A-ECF3079E4AC6}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{B9872419-4EB2-4B92-95FE-8FCC68C60D2B}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_8</Property>
	<Property Name="varPersistentID:{B9E6FE46-177B-44BC-8581-249A0EC104EE}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_10</Property>
	<Property Name="varPersistentID:{BAE65278-94C9-490B-B391-7FF365CF1D73}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{BB31613B-3104-4C37-A61F-51E9509CEA79}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{BBE7CB13-DC1E-4D27-97DF-F6F55FB2D769}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorCode</Property>
	<Property Name="varPersistentID:{BCC03931-2B8F-4B03-A146-8C7BC7B3278B}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingInterval</Property>
	<Property Name="varPersistentID:{BCE7F644-98C6-4A81-A5C8-72881C5CF8A8}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{BD716EF2-6572-424E-AFE8-0D77FD139BAE}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_PollingIterations</Property>
	<Property Name="varPersistentID:{BDFF2152-3DC0-4F19-AE9A-4973F4BB8BA0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{BE07A23E-6A15-4063-925B-A0BB72295026}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_2</Property>
	<Property Name="varPersistentID:{BE1E23D2-A962-45FD-B842-90EBC48B274B}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_Mode</Property>
	<Property Name="varPersistentID:{C217FE07-171E-4D25-9FAA-CE6CAF4CF103}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_1</Property>
	<Property Name="varPersistentID:{C2BCB7ED-FADE-436F-A047-B793158A5FC1}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_PollingIterations</Property>
	<Property Name="varPersistentID:{C533FE65-D155-488A-BB80-F018747F2E8C}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_PollingInterval</Property>
	<Property Name="varPersistentID:{C5B9782C-0E7E-4E20-8B57-ECC722E4778E}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_5</Property>
	<Property Name="varPersistentID:{C6621B59-B81F-47F8-8677-898BDC7A0B8C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{C6B99786-ECD1-4742-96B6-EBB6B96472B7}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Source</Property>
	<Property Name="varPersistentID:{C78B94DB-250E-4F9D-8A7F-0D5FE40112D8}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AO_1</Property>
	<Property Name="varPersistentID:{C80423CD-CB13-4E88-8580-274252136563}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{C8A8384A-3D92-4653-93DE-BDE03C0CCF4D}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{C91CBCB6-B49B-42F8-A5EB-2F800CEE8BC0}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_Error</Property>
	<Property Name="varPersistentID:{C9FDD3FD-26DA-4A77-9BEF-E511BF10DCA6}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_PollingDeltaT</Property>
	<Property Name="varPersistentID:{CB2F30BC-F5E6-4B09-B297-760A655967A7}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Level_3</Property>
	<Property Name="varPersistentID:{CB3EF315-1CEC-4E01-A960-EFB717BD72FD}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{D06D3CD1-87C1-4FB5-8230-CF32BD48C286}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{D3146C62-8D84-418A-9E22-DAE2CCD9D779}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{D3772573-6B4F-4F66-8B19-B5A8CA087196}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_PollingCounter</Property>
	<Property Name="varPersistentID:{D4110EA1-A156-41EA-96D3-DA093F1E5A65}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_PollingTime</Property>
	<Property Name="varPersistentID:{D4ACABB1-6E1F-456B-BD20-3245E6E2EE3C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{D5D2DF58-F1F9-45E9-BBA7-C5901A6BF207}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{D64225B5-C0FC-4790-83A1-68E5ABAC9E39}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_9</Property>
	<Property Name="varPersistentID:{D693F62B-E71C-44A2-905E-34080DB7898A}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_ErrorCode</Property>
	<Property Name="varPersistentID:{D781511C-1D5D-4F21-8A5A-38F746079FBD}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AO_0</Property>
	<Property Name="varPersistentID:{D81E9C3D-6597-423A-B57F-EF60A6B9106C}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{D8797A9E-B1AE-41AE-B056-99F31E300ADF}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_DecimationFactor</Property>
	<Property Name="varPersistentID:{D87A9CA4-315D-4453-B3EA-D546362B84AE}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScopeProxy_Activate</Property>
	<Property Name="varPersistentID:{D900C355-5295-479C-8A31-E11C89CA53B0}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_6</Property>
	<Property Name="varPersistentID:{D954C63F-2142-45F5-BC12-7DB2DA841150}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_ErrorStatus</Property>
	<Property Name="varPersistentID:{D971C09B-5BB5-429E-9007-104E76ED1508}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Error</Property>
	<Property Name="varPersistentID:{DA28AB5B-1AA1-4324-8241-4B79D1842D1C}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{DA2DBEC3-9616-4AC4-8BD4-7A763A41B102}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_ErrorMessage</Property>
	<Property Name="varPersistentID:{DA5BAC09-1A24-43CA-BC19-7EF6192B711A}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/BeepProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{DB287A40-1A71-4963-9775-74269F2116CE}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{DB8F4682-39FD-4CD0-A3B3-45B595BDFB2A}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{DCBBC34B-C57F-4BD9-9378-01BC5792B9E5}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_2</Property>
	<Property Name="varPersistentID:{DD376FD9-52B1-47EE-B6F0-45AA72B27828}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_ErrorMessage</Property>
	<Property Name="varPersistentID:{DF592E70-A4FB-4134-A287-55E52A1706C3}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_Streaming</Property>
	<Property Name="varPersistentID:{DF7E3AC8-8860-4B45-97A1-86A67FC60DCF}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{DF959671-FDE0-41E1-8305-F91A17982C59}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{DFD2BA89-33F9-4851-A963-C49A5A387B25}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{E0193DEF-8D6B-4D98-A0FD-954AC807DF58}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_Set-DO_11</Property>
	<Property Name="varPersistentID:{E06B507B-9E7D-469B-A831-5FEE21E6FA3D}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_MininumSamples</Property>
	<Property Name="varPersistentID:{E0C74B09-8FB6-4193-8FCB-0F9B8B5A97DE}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{E0FB2269-3BAA-40A1-B8F9-B2870AF8EFCB}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Window_MaxWidth</Property>
	<Property Name="varPersistentID:{E27F1048-F66F-41EF-9B3E-901724E0A595}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_ErrorMessage</Property>
	<Property Name="varPersistentID:{E51EBC97-FE2D-43D9-883E-C2F4B46A8EB7}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/Watchdog_PollingDeltaT</Property>
	<Property Name="varPersistentID:{E55EDF91-5622-4701-80E1-DA4EA0101251}" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities_SV.lvlib/Beep_ErrorMessage</Property>
	<Property Name="varPersistentID:{E7DF3CA3-5817-44E5-9833-B898766A5AD2}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{EA89D0A5-823D-49C7-B251-877B9720DF27}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/DeviceActorProxy_Activate</Property>
	<Property Name="varPersistentID:{EBB340A6-AB02-4A35-99D0-9FFD744A27B7}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncher_ErrorCode</Property>
	<Property Name="varPersistentID:{EC3AAD5D-E98D-4DDA-87FB-B9E141872888}" Type="Ref">/My Computer/SV.lib/CSPP_Core_SV.lvlib/BaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{EC95B01C-1AD4-470E-A56B-62B3B7D03571}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_8</Property>
	<Property Name="varPersistentID:{EDC36392-FED5-4D0C-8DF9-3AFFED434D85}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{EE32A47B-FFE4-4CC6-931C-A5932502BC6A}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{EEA2E511-858E-4E37-B3A4-16859FBF83EB}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/VCSLauncherProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{EF198B76-4133-4D79-8A88-88BD6C9C652F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIOProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{EF637EC8-3213-45EF-9B0D-E8ACA8DAE2D5}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_State</Property>
	<Property Name="varPersistentID:{EF905C2E-7B1F-41FB-9DEF-6BC740B21111}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Trigger_Hysteresis_4</Property>
	<Property Name="varPersistentID:{F062A540-CDD5-4F92-8BF4-B26E8CC8F88C}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{F11248B2-D5EF-4A37-9A83-D733162CE50D}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Watchdog_PollingInterval</Property>
	<Property Name="varPersistentID:{F2C58C3C-39AB-44BA-AD73-243524EFB567}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DI_0</Property>
	<Property Name="varPersistentID:{F32215DD-6DB3-4C52-88E3-858769E1C6C7}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{F3FE0D4C-2B9E-4B78-BD3D-91BC36AFEFEE}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_SelfTest</Property>
	<Property Name="varPersistentID:{F60841D5-A748-4F70-A65B-EE98F20B89C2}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{F6B80B5D-C71D-4FC6-A5E4-1E2FC3938ECF}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{F7A041C9-6CF9-4C82-BB22-8141CD9A1EB4}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_6</Property>
	<Property Name="varPersistentID:{F9A64AA9-8693-455B-8E8A-F1F15EC3CCA5}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_Acquisition_SamplesAcquired</Property>
	<Property Name="varPersistentID:{F9BB8D21-A114-4D3D-8E7F-FAC3CB0050EC}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{FA3C4AF4-F278-4191-91AC-6272FDEF0A06}" Type="Ref">/My Computer/SV.lib/VITOScope.lvlib/VITOScope_ErrorCode</Property>
	<Property Name="varPersistentID:{FA913142-76DE-4905-859F-C3E4287166A5}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_DO_9</Property>
	<Property Name="varPersistentID:{FB928F31-DDBD-4927-9A1E-5E1DDC322BDD}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_5</Property>
	<Property Name="varPersistentID:{FC453073-D0AA-411C-99F7-C37CFA236425}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{FCF72D76-A50E-4483-A590-83B1225257F9}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{FEB1EDEF-0128-4A19-BE4F-7A2544F2A2E9}" Type="Ref">/My Computer/SV.lib/VCS.lvlib/Beep_Error</Property>
	<Property Name="varPersistentID:{FF792D88-E7F4-488F-AE47-DED2E14836D3}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_PollingInterval</Property>
	<Property Name="varPersistentID:{FF7A0990-1DE0-45EE-B4FF-0BC60A14496F}" Type="Ref">/My Computer/SV.lib/SlowADIO.lvlib/SlowADIO_AI_13</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
			<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
		</Item>
		<Item Name="Documentation" Type="Folder"/>
		<Item Name="EUPL License" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
		</Item>
		<Item Name="HTV" Type="Folder">
			<Item Name="VCS-HTV-System.xml" Type="Document" URL="../HTV/VCS-HTV-System.xml"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="Device Session.lvclass" Type="LVClass" URL="../instr.lib/NI-FPGA-VITO/Host/Device Session/Device Session.lvclass"/>
			<Item Name="Main (NI PXIe-5170R 4CH).lvbitx" Type="Document" URL="../instr.lib/NI-FPGA-VITO/FPGA Bitfiles/Main (NI PXIe-5170R 4CH).lvbitx"/>
			<Item Name="RS-SMY_Drivers.lvlib" Type="Library" URL="../instr.lib/rhode-schwarz-smy/RS-SMY_Drivers.lvlib"/>
			<Item Name="VITO_Scope_Host.lvlib" Type="Library" URL="../instr.lib/NI-FPGA-VITO/Host/VITO/VITO_Scope_Host.lvlib"/>
		</Item>
		<Item Name="Packages" Type="Folder">
			<Item Name="BNT" Type="Folder">
				<Item Name="BNT_DAQmx-Content.vi" Type="VI" URL="../Packages/BNT_DAQmx/BNT_DAQmx-Content.vi"/>
				<Item Name="BNT_DAQmx.lvlib" Type="Library" URL="../Packages/BNT_DAQmx/BNT_DAQmx.lvlib"/>
				<Item Name="README.md" Type="Document" URL="../Packages/BNT_DAQmx/README.md"/>
			</Item>
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
					<Item Name="CSPP_QMsgLogMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_QMsgLogMonitor/CSPP_QMsgLogMonitor.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
					<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_Core/Change_Log.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_Core/Release_Notes.txt"/>
					<Item Name="VI-Analyzer-Configuration.cfg" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Configuration.cfg"/>
					<Item Name="VI-Analyzer-Results.rsl" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Results.rsl"/>
					<Item Name="VI-Analyzer-Spelling-Exceptions.txt" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Spelling-Exceptions.txt"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_NAInitialized Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_NAInitialized Msg/CSPP_NAInitialized Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
					<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_CoreContent-Linux.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent-Linux.vi"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="CSPP_DeploymentExample.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_DeploymentExample.vi"/>
				<Item Name="CSPP_Post-Build Action.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_Post-Build Action.vi"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
				<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
				<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
				<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
				<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
			</Item>
			<Item Name="gitversioninfo" Type="Folder">
				<Item Name="GitControl.lvlib" Type="Library" URL="../Packages/gitversioninfo/GitControl.lvlib"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
				<Item Name="CSPP_ObjectManager_SV.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_SV.lvlib"/>
			</Item>
			<Item Name="RS-SMY" Type="Folder">
				<Item Name="RS-SMY.lvlib" Type="Library" URL="../Packages/RS-SMY/RS-SMY.lvlib"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
				<Item Name="CSPP_SystemMonitor_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/CSPP_SystemMonitor_SV.lvlib"/>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_Utilities_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/CSPP_Utilities_SV.lvlib"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
			<Item Name="VITO" Type="Folder">
				<Item Name="CSPP_VITO_Content.vi" Type="VI" URL="../Packages/CSPP_VITO/CSPP_VITO_Content.vi"/>
				<Item Name="RF_def.ctl" Type="VI" URL="../RF_def.ctl"/>
				<Item Name="VCS_Launcher.lvlib" Type="Library" URL="../Packages/CSPP_VITO/VCS_Launcher/VCS_Launcher.lvlib"/>
				<Item Name="VITO_Scope.lvlib" Type="Library" URL="../Packages/CSPP_VITO/VITO_Scope/VITO_Scope.lvlib"/>
			</Item>
		</Item>
		<Item Name="SV.lib" Type="Folder">
			<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
			<Item Name="SlowADIO.lvlib" Type="Library" URL="../SV.lib/SlowADIO.lvlib"/>
			<Item Name="VCS.lvlib" Type="Library" URL="../SV.lib/VCS.lvlib"/>
			<Item Name="VITOScope.lvlib" Type="Library" URL="../Packages/CSPP_VITO/VITO_Scope/VITOScope.lvlib"/>
		</Item>
		<Item Name="User" Type="Folder"/>
		<Item Name="CSPP-Minimum.ini" Type="Document" URL="../CSPP-Minimum.ini"/>
		<Item Name="README.md" Type="Document" URL="../README.md"/>
		<Item Name="Release_Notes.md" Type="Document" URL="../Release_Notes.md"/>
		<Item Name="VCS.ico" Type="Document" URL="../VCS.ico"/>
		<Item Name="VCS.ini" Type="Document" URL="../VCS.ini"/>
		<Item Name="VCS_Content.vi" Type="VI" URL="../VCS_Content.vi"/>
		<Item Name="VCS_Main.vi" Type="VI" URL="../VCS_Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="DFlopBasicElements_0B830B7E81994019B09E50CE7A175B36.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/DFlopBEResetSimFiles/DFlopBasicElements_0B830B7E81994019B09E50CE7A175B36.dll"/>
				<Item Name="DFlopBasicElements_C792537791DE412E8810E138F5BC4696.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/DFlopBEPresetSimFiles/DFlopBasicElements_C792537791DE412E8810E138F5BC4696.dll"/>
				<Item Name="DoubleSyncBasicElements_C792537791DE412E8810E138F5BC4696.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/DblSyncBEResetSimFiles/DoubleSyncBasicElements_C792537791DE412E8810E138F5BC4696.dll"/>
				<Item Name="EqParallelComplexTop_592D770CDFE541E99D96194ACDC69DD8.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/EqParallel/EqpCplx16Spc16CoefSimFiles/EqParallelComplexTop_592D770CDFE541E99D96194ACDC69DD8.dll"/>
				<Item Name="EqParallelTop_592D770CDFE541E99D96194ACDC69DD8.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/EqParallel/EqParallel16Spc32CoefSimFiles/EqParallelTop_592D770CDFE541E99D96194ACDC69DD8.dll"/>
				<Item Name="EqpSingleFirTop_592D770CDFE541E99D96194ACDC69DD8.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/EqParallel/EqpSingleFir16spc16coefSimFiles/EqpSingleFirTop_592D770CDFE541E99D96194ACDC69DD8.dll"/>
				<Item Name="EqSingleFilter_597026864DC13FFFA120CE82B9307E2D.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Equalization/FIRFilter_1spcSimFiles/EqSingleFilter_597026864DC13FFFA120CE82B9307E2D.dll"/>
				<Item Name="FourInputGlitchFreeMuxBasicElements_B15BA4892E5F4023A51AA2E61B6FD011.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/GlitchlessMux_4InputSimFiles/FourInputGlitchFreeMuxBasicElements_B15BA4892E5F4023A51AA2E61B6FD011.dll"/>
				<Item Name="FractDecFirParTop_269921035B2F48F788588C5E39925AB3.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/FirParallel/FractDec8spcParSimFiles/FractDecFirParTop_269921035B2F48F788588C5E39925AB3.dll"/>
				<Item Name="FractDecProcBlockTopSLV_742A75B84858410DE7ADB3A1328C798F.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Decimator/FDPB1spc2x1pSimFiles/FractDecProcBlockTopSLV_742A75B84858410DE7ADB3A1328C798F.dll"/>
				<Item Name="FractDecProcBlockTopSLV_DD802FF575B148B59F51A1B8F98BF04B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Decimator/FDPB1spc3x2pbSimFiles/FractDecProcBlockTopSLV_DD802FF575B148B59F51A1B8F98BF04B.dll"/>
				<Item Name="FractInterpFirParTop_269921035B2F48F788588C5E39925AB3.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/FirParallel/FractInterp8spcParSimFiles/FractInterpFirParTop_269921035B2F48F788588C5E39925AB3.dll"/>
				<Item Name="FractInterpProcBlockTopSLV_20F1EBC4B3834F5BA53E63A264F34FB7.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB2spc2xOCSimFiles/FractInterpProcBlockTopSLV_20F1EBC4B3834F5BA53E63A264F34FB7.dll"/>
				<Item Name="FractInterpProcBlockTopSLV_44C625AB2C454E31B741FCDC5D44EB6D.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB2spc3xOCSimFiles/FractInterpProcBlockTopSLV_44C625AB2C454E31B741FCDC5D44EB6D.dll"/>
				<Item Name="FractInterpProcBlockTopSLV_5460344A88624817A0233397F33A245E.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB1spc2xOCSimFiles/FractInterpProcBlockTopSLV_5460344A88624817A0233397F33A245E.dll"/>
				<Item Name="FractInterpProcBlockTopSLV_DBC7C48583F34D52BAD9E80D6EDF48B3.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB8spc2xOCSimFiles/FractInterpProcBlockTopSLV_DBC7C48583F34D52BAD9E80D6EDF48B3.dll"/>
				<Item Name="FractInterpProcBlockTopSLV_EDB25292FA1846F08B600B042B500A35.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB4spc2xOCSimFiles/FractInterpProcBlockTopSLV_EDB25292FA1846F08B600B042B500A35.dll"/>
				<Item Name="FractInterpProcBlockTopSLV_F7B9FA9A94284E879631B341D8E91039.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB1spc3xOCSimFiles/FractInterpProcBlockTopSLV_F7B9FA9A94284E879631B341D8E91039.dll"/>
				<Item Name="HbDecMultipleInputSpc2xOcIpin_8C54370188DE43B0A7321F38B0D3F02C.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd1spc2xV5SimFiles/HbDecMultipleInputSpc2xOcIpin_8C54370188DE43B0A7321F38B0D3F02C.dll"/>
				<Item Name="HbDecMultipleInputSpc2xOcIpin_71B41EE54E06E5419F11B48861257C5B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd16spc2xSimFiles/HbDecMultipleInputSpc2xOcIpin_71B41EE54E06E5419F11B48861257C5B.dll"/>
				<Item Name="HbDecMultipleInputSpc2xOcIpin_623BD1B12A0B4CC9A38C7F3C325244B4.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd8spc2xV5SimFiles/HbDecMultipleInputSpc2xOcIpin_623BD1B12A0B4CC9A38C7F3C325244B4.dll"/>
				<Item Name="HbDecMultipleInputSpc2xOcIpin_6166FF3625F34A3A98FE65A4EF5C4131.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd4spc2xV5SimFiles/HbDecMultipleInputSpc2xOcIpin_6166FF3625F34A3A98FE65A4EF5C4131.dll"/>
				<Item Name="HbDecMultipleInputSpc2xOcIpin_EBF1868819454DDA96F718AC69E8AC23.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd16spc2xV5SimFiles/HbDecMultipleInputSpc2xOcIpin_EBF1868819454DDA96F718AC69E8AC23.dll"/>
				<Item Name="HbDecMultipleInputSpc2xOcIpin_F77ED40E484009972895A0944AD0710B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd1spc2xSimFiles/HbDecMultipleInputSpc2xOcIpin_F77ED40E484009972895A0944AD0710B.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_6C9BD6BA82F44DA8A08D75EF2AE906AE.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4cps2xV5SimFiles/HbInterpMultipleInputSpc2xOcIpin_6C9BD6BA82F44DA8A08D75EF2AE906AE.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_6D0DC94E93A040DE87EFA49BF4DD943A.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4cps2xSimFiles/HbInterpMultipleInputSpc2xOcIpin_6D0DC94E93A040DE87EFA49BF4DD943A.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_19BA91DC1C4244D8B8DECEC0EC964B88.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4spc2xSimFiles/HbInterpMultipleInputSpc2xOcIpin_19BA91DC1C4244D8B8DECEC0EC964B88.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_82A86AF20A8549408D70354033B2CD5D.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2cps2xSimFiles/HbInterpMultipleInputSpc2xOcIpin_82A86AF20A8549408D70354033B2CD5D.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_647E9650F17C45DAB5FD18A83D2B6D9B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp1spc2xV5SimFiles/HbInterpMultipleInputSpc2xOcIpin_647E9650F17C45DAB5FD18A83D2B6D9B.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_8909ED0E4A4245BF8835625EC34AEA8B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2cps2xV5SimFiles/HbInterpMultipleInputSpc2xOcIpin_8909ED0E4A4245BF8835625EC34AEA8B.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_A349CE296BA2485A8D35EF60F4F43469.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp8spc2xSimFiles/HbInterpMultipleInputSpc2xOcIpin_A349CE296BA2485A8D35EF60F4F43469.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_B1FAAED049E842E09D4F5011FBB62A25.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4spc2xV5SimFiles/HbInterpMultipleInputSpc2xOcIpin_B1FAAED049E842E09D4F5011FBB62A25.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_BDA8949F7FA64E0D877C445B591D2EF0.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2spc2xSimFiles/HbInterpMultipleInputSpc2xOcIpin_BDA8949F7FA64E0D877C445B591D2EF0.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_C27E343B8BAB4DDCBD75AFB5A16ED8C7.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp8spc2xV5SimFiles/HbInterpMultipleInputSpc2xOcIpin_C27E343B8BAB4DDCBD75AFB5A16ED8C7.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_F1C5885371804FA18C8488B03DBC0753.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2spc2xV5SimFiles/HbInterpMultipleInputSpc2xOcIpin_F1C5885371804FA18C8488B03DBC0753.dll"/>
				<Item Name="HbInterpMultipleInputSpc2xOcIpin_F77ED40E484009972895A0944AD0710B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp1spc2xSimFiles/HbInterpMultipleInputSpc2xOcIpin_F77ED40E484009972895A0944AD0710B.dll"/>
				<Item Name="MaxFanoutFf_6A315D12FBC743E4ACD121A1951E87AC.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_8SimFiles/MaxFanoutFf_6A315D12FBC743E4ACD121A1951E87AC.dll"/>
				<Item Name="MaxFanoutFf_417FC7712F0F4A3C95450BAF224B3F38.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_32SimFiles/MaxFanoutFf_417FC7712F0F4A3C95450BAF224B3F38.dll"/>
				<Item Name="MaxFanoutFf_36915C3B0A964A738AD3121E87CF92C2.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_16SimFiles/MaxFanoutFf_36915C3B0A964A738AD3121E87CF92C2.dll"/>
				<Item Name="MaxFanoutFf_E0EDB5E65B8B49DD8E163F5688E824C1.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_4SimFiles/MaxFanoutFf_E0EDB5E65B8B49DD8E163F5688E824C1.dll"/>
				<Item Name="ni5170CalData.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/ni5170CalData/ni5170CalData.lvlib"/>
				<Item Name="niHSAI Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/Cal Data/niHSAI Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Common Config v1 Host.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Config/Common/niHSAI Common Config v1 Host.lvlib"/>
				<Item Name="niHSAI Compensated Attenuator Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/Compensated Attenuator/niHSAI Compensated Attenuator Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Config v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Config/Interface/v1/Host/niHSAI Config v1 Host.lvclass"/>
				<Item Name="niHSAI DC Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/DC/niHSAI DC Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI DC Cal Data v2 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v2/Host/DC/niHSAI DC Cal Data v2 Host.lvclass"/>
				<Item Name="niHSAI Group A Base Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A Base/v1/Host/Cal Data/niHSAI Group A Base Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A/v1/Host/Cal Data/niHSAI Group A Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A Config v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Config/Group A/v1/FPGA/niHSAI Group A Config v1 FPGA.lvlib"/>
				<Item Name="niHSAI Group A Config v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Config/Group A/v1/Host/Main/niHSAI Group A Config v1 Host.lvclass"/>
				<Item Name="niHSAI Group A DC Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A/v1/Host/DC/niHSAI Group A DC Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A Phase Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A/v1/Host/Phase/niHSAI Group A Phase Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A TDC Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A/v1/Host/TDC/niHSAI Group A TDC Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A Timebase Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A/v1/Host/Timebase/niHSAI Group A Timebase Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A1 Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/Cal Data/niHSAI Group A1 Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A1 Cal Map v1 Shared Private.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/Cal Map/niHSAI Group A1 Cal Map v1 Shared Private.lvlib"/>
				<Item Name="niHSAI Group A1 Compensated Attenuator Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/Compensated Attenuator/niHSAI Group A1 Compensated Attenuator Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A1 DC Cal Data v2 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v2/Host/DC/niHSAI Group A1 DC Cal Data v2 Host.lvclass"/>
				<Item Name="niHSAI Group A1 Offset Dac Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/Offset Dac/niHSAI Group A1 Offset Dac Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A1 Phase Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/Phase/niHSAI Group A1 Phase Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A1 TDC Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/TDC/niHSAI Group A1 TDC Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Group A1 Timebase Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Group A1/v1/Host/Timebase/niHSAI Group A1 Timebase Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Memory FIFO DRAM U384x2 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Memory FIFO/v1/FPGA/DRAM/U384x2/niHSAI Memory FIFO DRAM U384x2 v1 FPGA.lvclass"/>
				<Item Name="niHSAI Memory FIFO DRAM v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Memory FIFO/v1/FPGA/DRAM/Base/niHSAI Memory FIFO DRAM v1 FPGA.lvclass"/>
				<Item Name="niHSAI Memory FIFO v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Memory FIFO/v1/FPGA/Base/niHSAI Memory FIFO v1 FPGA.lvclass"/>
				<Item Name="niHSAI Offset Dac Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/Offset Dac/niHSAI Offset Dac Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Phase Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/Phase/niHSAI Phase Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI TDC Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/TDC/niHSAI TDC Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Timebase Cal Data v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/HSAI-RIO/Cal Data/Interface/v1/Host/Timebase/niHSAI Timebase Cal Data v1 Host.lvclass"/>
				<Item Name="niHSAI Types v1 Host.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Types/v1/Host/niHSAI Types v1 Host.lvlib"/>
				<Item Name="niHSAI Utilities v1 Host.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Utilities/v1/Host/niHSAI Utilities v1 Host.lvlib"/>
				<Item Name="niHsaiGroupAConfig.lvlib" Type="Library" URL="/&lt;instrlib&gt;/HSAI-RIO/Config/niHsaiGroupAConfig/niHsaiGroupAConfig.lvlib"/>
				<Item Name="niInstr Basic Elements v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/niInstr Basic Elements v1 FPGA.lvlib"/>
				<Item Name="niInstr Channel v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Channel/v1/FPGA/niInstr Channel v1 FPGA.lvlib"/>
				<Item Name="niInstr Data Formatting v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Data Formatting/v1/FPGA/niInstr Data Formatting v1 FPGA.lvlib"/>
				<Item Name="niInstr Data Manipulation v1 Host.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Data Manipulation/v1/Host/niInstr Data Manipulation v1 Host.lvlib"/>
				<Item Name="niInstr Data Trigger v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Data Trigger/v1/FPGA/niInstr Data Trigger v1 FPGA.lvlib"/>
				<Item Name="niInstr Data Trigger v1 Shared.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Data Trigger/v1/Shared/niInstr Data Trigger v1 Shared.lvlib"/>
				<Item Name="niInstr DSP v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/niInstr DSP v1 FPGA.lvlib"/>
				<Item Name="niInstr DSP v1 Shared.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/Shared/niInstr DSP v1 Shared.lvlib"/>
				<Item Name="niInstr FIFO Register Bus v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/FIFO Register Bus/v1/FPGA/niInstr FIFO Register Bus v1 FPGA.lvclass"/>
				<Item Name="niInstr FIFO Register Bus v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/FIFO Register Bus/v1/Host/niInstr FIFO Register Bus v1 Host.lvclass"/>
				<Item Name="niInstr FIFO Register Bus v1 Shared.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/FIFO Register Bus/v1/Shared/niInstr FIFO Register Bus v1 Shared.lvlib"/>
				<Item Name="niInstr Flattened Address Space v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/FPGA/Flattened Address Space/niInstr Flattened Address Space v1 FPGA.lvlib"/>
				<Item Name="niInstr Instruction Framework Context v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/Host/Instruction Framework Context/niInstr Instruction Framework Context v1 Host.lvclass"/>
				<Item Name="niInstr Instruction Framework Interfaces v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/FPGA/Interfaces/niInstr Instruction Framework Interfaces v1 FPGA.lvlib"/>
				<Item Name="niInstr Instruction Framework Top Level Bus v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/FPGA/Top Level Bus/niInstr Instruction Framework Top Level Bus v1 FPGA.lvlib"/>
				<Item Name="niInstr Instruction Framework v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/FPGA/Typedefs/niInstr Instruction Framework v1 FPGA.lvlib"/>
				<Item Name="niInstr Instruction Target v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/Host/Instruction Target/niInstr Instruction Target v1 Host.lvclass"/>
				<Item Name="niInstr Memory Arbiter Interface v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM Arbiter/Interface/niInstr Memory Arbiter Interface v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U64 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U64/niInstr Memory BRAM U64 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U128 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U128/niInstr Memory BRAM U128 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U256 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U256/niInstr Memory BRAM U256 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U384 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U384/niInstr Memory BRAM U384 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U512 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U512/niInstr Memory BRAM U512 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U768 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U768/niInstr Memory BRAM U768 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM U1024 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM U1024/niInstr Memory BRAM U1024 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory BRAM v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/BRAM/niInstr Memory BRAM v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U64 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U64/niInstr Memory DRAM U64 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U128 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U128/niInstr Memory DRAM U128 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U256 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U256/niInstr Memory DRAM U256 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U384 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U384/niInstr Memory DRAM U384 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U512 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U512/niInstr Memory DRAM U512 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U1024 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U1024/niInstr Memory DRAM U1024 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM U1280 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM U1280/niInstr Memory DRAM U1280 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAM v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM/niInstr Memory DRAM v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory DRAMx2 v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAMx2/niInstr Memory DRAMx2 v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory Round Robin Arbiter v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/DRAM Arbiter/Round Robin/niInstr Memory Round Robin Arbiter v1 FPGA.lvclass"/>
				<Item Name="niInstr Memory v1 FPGA.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Memory/v1/FPGA/Memory/niInstr Memory v1 FPGA.lvclass"/>
				<Item Name="niInstr Subsystem Map v1 Host.lvclass" Type="LVClass" URL="/&lt;instrlib&gt;/_niInstr/Instruction Framework/v1/Host/Subsystem Map/niInstr Subsystem Map v1 Host.lvclass"/>
				<Item Name="niInstr Types v1 Shared.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Types/v1/Shared/niInstr Types v1 Shared.lvlib"/>
				<Item Name="xsimk0B830B7E81994019B09E50CE7A175B36.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/DFlopBEResetSimFiles/xsim.dir/DFlopBasicElements/xsimk0B830B7E81994019B09E50CE7A175B36.dll"/>
				<Item Name="xsimk6A315D12FBC743E4ACD121A1951E87AC.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_8SimFiles/xsim.dir/MaxFanoutFf/xsimk6A315D12FBC743E4ACD121A1951E87AC.dll"/>
				<Item Name="xsimk6C9BD6BA82F44DA8A08D75EF2AE906AE.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4cps2xV5SimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimk6C9BD6BA82F44DA8A08D75EF2AE906AE.dll"/>
				<Item Name="xsimk6D0DC94E93A040DE87EFA49BF4DD943A.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4cps2xSimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimk6D0DC94E93A040DE87EFA49BF4DD943A.dll"/>
				<Item Name="xsimk8C54370188DE43B0A7321F38B0D3F02C.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd1spc2xV5SimFiles/xsim.dir/HbDecMultipleInputSpc2xOcIpin/xsimk8C54370188DE43B0A7321F38B0D3F02C.dll"/>
				<Item Name="xsimk19BA91DC1C4244D8B8DECEC0EC964B88.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4spc2xSimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimk19BA91DC1C4244D8B8DECEC0EC964B88.dll"/>
				<Item Name="xsimk20F1EBC4B3834F5BA53E63A264F34FB7.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB2spc2xOCSimFiles/xsim.dir/FractInterpProcBlockTopSLV/xsimk20F1EBC4B3834F5BA53E63A264F34FB7.dll"/>
				<Item Name="xsimk44C625AB2C454E31B741FCDC5D44EB6D.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB2spc3xOCSimFiles/xsim.dir/FractInterpProcBlockTopSLV/xsimk44C625AB2C454E31B741FCDC5D44EB6D.dll"/>
				<Item Name="xsimk71B41EE54E06E5419F11B48861257C5B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd16spc2xSimFiles/xsim.dir/HbDecMultipleInputSpc2xOcIpin/xsimk71B41EE54E06E5419F11B48861257C5B.dll"/>
				<Item Name="xsimk82A86AF20A8549408D70354033B2CD5D.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2cps2xSimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimk82A86AF20A8549408D70354033B2CD5D.dll"/>
				<Item Name="xsimk417FC7712F0F4A3C95450BAF224B3F38.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_32SimFiles/xsim.dir/MaxFanoutFf/xsimk417FC7712F0F4A3C95450BAF224B3F38.dll"/>
				<Item Name="xsimk592D770CDFE541E99D96194ACDC69DD8.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/EqParallel/EqParallel16Spc32CoefSimFiles/xsim.dir/EqParallelTop/xsimk592D770CDFE541E99D96194ACDC69DD8.dll"/>
				<Item Name="xsimk623BD1B12A0B4CC9A38C7F3C325244B4.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd8spc2xV5SimFiles/xsim.dir/HbDecMultipleInputSpc2xOcIpin/xsimk623BD1B12A0B4CC9A38C7F3C325244B4.dll"/>
				<Item Name="xsimk647E9650F17C45DAB5FD18A83D2B6D9B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp1spc2xV5SimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimk647E9650F17C45DAB5FD18A83D2B6D9B.dll"/>
				<Item Name="xsimk742A75B84858410DE7ADB3A1328C798F.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Decimator/FDPB1spc2x1pSimFiles/xsim.dir/FractDecProcBlockTopSLV/xsimk742A75B84858410DE7ADB3A1328C798F.dll"/>
				<Item Name="xsimk6166FF3625F34A3A98FE65A4EF5C4131.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd4spc2xV5SimFiles/xsim.dir/HbDecMultipleInputSpc2xOcIpin/xsimk6166FF3625F34A3A98FE65A4EF5C4131.dll"/>
				<Item Name="xsimk8909ED0E4A4245BF8835625EC34AEA8B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2cps2xV5SimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimk8909ED0E4A4245BF8835625EC34AEA8B.dll"/>
				<Item Name="xsimk36915C3B0A964A738AD3121E87CF92C2.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_16SimFiles/xsim.dir/MaxFanoutFf/xsimk36915C3B0A964A738AD3121E87CF92C2.dll"/>
				<Item Name="xsimk5460344A88624817A0233397F33A245E.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB1spc2xOCSimFiles/xsim.dir/FractInterpProcBlockTopSLV/xsimk5460344A88624817A0233397F33A245E.dll"/>
				<Item Name="xsimk269921035B2F48F788588C5E39925AB3.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/FirParallel/FractDec8spcParSimFiles/xsim.dir/FractDecFirParTop/xsimk269921035B2F48F788588C5E39925AB3.dll"/>
				<Item Name="xsimk597026864DC13FFFA120CE82B9307E2D.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Equalization/FIRFilter_1spcSimFiles/xsim.dir/EqSingleFilter/xsimk597026864DC13FFFA120CE82B9307E2D.dll"/>
				<Item Name="xsimkA349CE296BA2485A8D35EF60F4F43469.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp8spc2xSimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimkA349CE296BA2485A8D35EF60F4F43469.dll"/>
				<Item Name="xsimkB1FAAED049E842E09D4F5011FBB62A25.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp4spc2xV5SimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimkB1FAAED049E842E09D4F5011FBB62A25.dll"/>
				<Item Name="xsimkB15BA4892E5F4023A51AA2E61B6FD011.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/GlitchlessMux_4InputSimFiles/xsim.dir/FourInputGlitchFreeMuxBasicElements/xsimkB15BA4892E5F4023A51AA2E61B6FD011.dll"/>
				<Item Name="xsimkBDA8949F7FA64E0D877C445B591D2EF0.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2spc2xSimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimkBDA8949F7FA64E0D877C445B591D2EF0.dll"/>
				<Item Name="xsimkC27E343B8BAB4DDCBD75AFB5A16ED8C7.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp8spc2xV5SimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimkC27E343B8BAB4DDCBD75AFB5A16ED8C7.dll"/>
				<Item Name="xsimkC792537791DE412E8810E138F5BC4696.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Private/DFlopBEPresetSimFiles/xsim.dir/DFlopBasicElements/xsimkC792537791DE412E8810E138F5BC4696.dll"/>
				<Item Name="xsimkDBC7C48583F34D52BAD9E80D6EDF48B3.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB8spc2xOCSimFiles/xsim.dir/FractInterpProcBlockTopSLV/xsimkDBC7C48583F34D52BAD9E80D6EDF48B3.dll"/>
				<Item Name="xsimkDD802FF575B148B59F51A1B8F98BF04B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Decimator/FDPB1spc3x2pbSimFiles/xsim.dir/FractDecProcBlockTopSLV/xsimkDD802FF575B148B59F51A1B8F98BF04B.dll"/>
				<Item Name="xsimkE0EDB5E65B8B49DD8E163F5688E824C1.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/Public/ModGen/ff_max_fanout_4SimFiles/xsim.dir/MaxFanoutFf/xsimkE0EDB5E65B8B49DD8E163F5688E824C1.dll"/>
				<Item Name="xsimkEBF1868819454DDA96F718AC69E8AC23.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd16spc2xV5SimFiles/xsim.dir/HbDecMultipleInputSpc2xOcIpin/xsimkEBF1868819454DDA96F718AC69E8AC23.dll"/>
				<Item Name="xsimkEDB25292FA1846F08B600B042B500A35.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB4spc2xOCSimFiles/xsim.dir/FractInterpProcBlockTopSLV/xsimkEDB25292FA1846F08B600B042B500A35.dll"/>
				<Item Name="xsimkF1C5885371804FA18C8488B03DBC0753.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Interpolator/HbInterp2spc2xV5SimFiles/xsim.dir/HbInterpMultipleInputSpc2xOcIpin/xsimkF1C5885371804FA18C8488B03DBC0753.dll"/>
				<Item Name="xsimkF7B9FA9A94284E879631B341D8E91039.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/Fractional Interpolator/FIPB1spc3xOCSimFiles/xsim.dir/FractInterpProcBlockTopSLV/xsimkF7B9FA9A94284E879631B341D8E91039.dll"/>
				<Item Name="xsimkF77ED40E484009972895A0944AD0710B.dll" Type="Document" URL="/&lt;instrlib&gt;/_niInstr/DSP/v1/FPGA/Private/HB Decimator/Hbd1spc2xSimFiles/xsim.dir/HbDecMultipleInputSpc2xOcIpin/xsimkF77ED40E484009972895A0944AD0710B.dll"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="1D String Array to Delimited String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/1D String Array to Delimited String.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="ALM_Alarm_Status.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Alarm_Status.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Make_Summary.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Make_Summary.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="ALM_Test_For_Valid_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Test_For_Valid_Alarm.vi"/>
				<Item Name="ALM_Test_Zero.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Test_Zero.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="AsciiToInt.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/AsciiToInt.vi"/>
				<Item Name="Batch Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Casting Utility For Actors.vim" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Actor/Casting Utility For Actors.vim"/>
				<Item Name="cfis_Get File Extension Without Changing Case.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Get File Extension Without Changing Case.vi"/>
				<Item Name="cfis_Replace Percent Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Replace Percent Code.vi"/>
				<Item Name="cfis_Reverse Scan From String For Integer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Reverse Scan From String For Integer.vi"/>
				<Item Name="cfis_Split File Path Into Three Parts.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/cfis_Split File Path Into Three Parts.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check for multiple of dt.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for multiple of dt.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Clear-68016.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/Clear-68016.vi"/>
				<Item Name="ClearError.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ClearError.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="configureNumberOfValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/configureNumberOfValues.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create File with Incrementing Suffix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Create File with Incrementing Suffix.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Control Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Control Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Digital Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Subset.vi"/>
				<Item Name="DTbl Get Digital Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Get Digital Value.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DWDT Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Subset.vi"/>
				<Item Name="DWDT Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get XY Value.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="Equal Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Comparable/Equal Comparable.lvclass"/>
				<Item Name="Equal Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Functor/Equal Functor.lvclass"/>
				<Item Name="Equals.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Equals.vim"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="ExtractSubstring.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ExtractSubstring.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="fileViewerConfigData.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/fileViewerConfigData.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="formatPropertyList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/formatPropertyList.vi"/>
				<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Subset.vi"/>
				<Item Name="Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get XY Value.vi"/>
				<Item Name="getChannelList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/getChannelList.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="getNamesFromPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/getNamesFromPath.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GoTo.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/GoTo.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="initFileContentsTree.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initFileContentsTree.vi"/>
				<Item Name="InitFromConfiguration.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/InitFromConfiguration.vi"/>
				<Item Name="initHelpButtonVisibility.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initHelpButtonVisibility.vi"/>
				<Item Name="InitScrollbarAndListBox.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/InitScrollbarAndListBox.vi"/>
				<Item Name="initTabValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initTabValues.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="loadAndFormatValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/loadAndFormatValues.vi"/>
				<Item Name="LoadBufferForMultiListBoxAndFormat.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/LoadBufferForMultiListBoxAndFormat.vi"/>
				<Item Name="LogicalSort.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/LogicalSort.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="ni5170 Driver Interface.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Targets/FPGA/Digitizers/Driver Interface/5170/ni5170 Driver Interface.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="niHSAI Plugin Interface.lvlib" Type="Library" URL="/&lt;vilib&gt;/LabVIEW Targets/FPGA/Digitizers/Plugin Interface/niHSAI Plugin Interface.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="panelResize_tdms.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/panelResize_tdms.vi"/>
				<Item Name="panelstate.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/panelstate.ctl"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Search Unsorted 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Search Unsorted 1D Array Core.vim"/>
				<Item Name="Search Unsorted 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Search Unsorted 1D Array.vim"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="setListBoxColumnWidths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/setListBoxColumnWidths.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="sizeaction.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/sizeaction.ctl"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/status.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TDMS - File Viewer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMS - File Viewer.vi"/>
				<Item Name="TDMSFileViewer_LaunchHelp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMSFileViewer_LaunchHelp.vi"/>
				<Item Name="TDMSFileViewerLocalizedText.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMSFileViewerLocalizedText.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="UpdateBufferForMultiListBoxIfNecessary.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateBufferForMultiListBoxIfNecessary.vi"/>
				<Item Name="UpdateListBoxAfterKeyEvent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateListBoxAfterKeyEvent.vi"/>
				<Item Name="UpdateScrollbarBeforeKeyEvent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateScrollbarBeforeKeyEvent.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="WDT Get Waveform Subset CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset CDB.vi"/>
				<Item Name="WDT Get Waveform Subset DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset DBL.vi"/>
				<Item Name="WDT Get Waveform Subset EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset EXT.vi"/>
				<Item Name="WDT Get Waveform Subset I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I8.vi"/>
				<Item Name="WDT Get Waveform Subset I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I16.vi"/>
				<Item Name="WDT Get Waveform Subset I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I32.vi"/>
				<Item Name="WDT Get Waveform Subset SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset SGL.vi"/>
				<Item Name="WDT Get XY Value CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CDB.vi"/>
				<Item Name="WDT Get XY Value CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CXT.vi"/>
				<Item Name="WDT Get XY Value DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value DBL.vi"/>
				<Item Name="WDT Get XY Value EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value EXT.vi"/>
				<Item Name="WDT Get XY Value I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I16.vi"/>
				<Item Name="WDT Get XY Value I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I32.vi"/>
				<Item Name="WDT Get XY Value I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I64.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="2 Element Array FXP(+-,14,1) Remove Overflow.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array FXP(+-,14,1) Remove Overflow.vi"/>
			<Item Name="2 Element Array FXP(+-,14,1) to FXP(+-,18,1).vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array FXP(+-,14,1) to FXP(+-,18,1).vi"/>
			<Item Name="2 Element Array FXP(+-,14,1) to I16.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array FXP(+-,14,1) to I16.vi"/>
			<Item Name="2 Element Array FXP(+-,18,1) Remove Overflow.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array FXP(+-,18,1) Remove Overflow.vi"/>
			<Item Name="2 Element Array FXP(+-,18,1) to FXP(+-,14,1).vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array FXP(+-,18,1) to FXP(+-,14,1).vi"/>
			<Item Name="2 Element Array FXP(+-,18,1) to I16.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array FXP(+-,18,1) to I16.vi"/>
			<Item Name="2 Element Array Overflow Support FXP(+-,18,1).vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/2 Element Array Overflow Support FXP(+-,18,1).vi"/>
			<Item Name="2 Element Generate Sine Cos Data FXP(+,-,14,1).vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Emulation/SubVIs/2 Element Generate Sine Cos Data FXP(+,-,14,1).vi"/>
			<Item Name="2 Element Generate Sine Cos Data FXP(+,-,18,1).vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Emulation/SubVIs/2 Element Generate Sine Cos Data FXP(+,-,18,1).vi"/>
			<Item Name="2 Element Generate Sine Cos Data I16.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Emulation/SubVIs/2 Element Generate Sine Cos Data I16.vi"/>
			<Item Name="2 Element Generate Sine Cos Data.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Emulation/2 Element Generate Sine Cos Data.vi"/>
			<Item Name="acquisition state signals.ctl" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Sub VIs/Typedef/acquisition state signals.ctl"/>
			<Item Name="acquisition state.ctl" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Sub VIs/Typedef/acquisition state.ctl"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Data Type Conversion FXP(+-,14,1) to FXP(+-,18,1)x4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,14,1) to FXP(+-,18,1)x4ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,14,1) to FXP(+-,18,1)x8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,14,1) to FXP(+-,18,1)x8ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,14,1) to I16x4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,14,1) to I16x4ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,14,1) to I16x8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,14,1) to I16x8ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,18,1) to FXP(+-,14,1)x4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,18,1) to FXP(+-,14,1)x4ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,18,1) to FXP(+-,14,1)x8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,18,1) to FXP(+-,14,1)x8ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,18,1) to I16x4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,18,1) to I16x4ch.vi"/>
			<Item Name="Data Type Conversion FXP(+-,18,1) to I16x8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Data Type Conversion FXP(+-,18,1) to I16x8ch.vi"/>
			<Item Name="Data Type Conversion.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/Data Type Conversion.vi"/>
			<Item Name="Integer decimation.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/DSP/Integer decimation.vi"/>
			<Item Name="Integer decimationx4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/DSP/SubVIs/Integer decimationx4ch.vi"/>
			<Item Name="Integer decimationx8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/DSP/SubVIs/Integer decimationx8ch.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Main-DPG (NI PXIe-5170R 4CH).lvbitx" Type="Document" URL="../instr.lib/NI-FPGA-VITO/FPGA Bitfiles/Main-DPG (NI PXIe-5170R 4CH).lvbitx"/>
			<Item Name="Main-DPG_init-CT-8P (NI PXIe-5170R 8CH)-CalcPPnew_GDT.lvbitx" Type="Document" URL="../instr.lib/NI-FPGA-VITO/FPGA Bitfiles/Main-DPG_init-CT-8P (NI PXIe-5170R 8CH)-CalcPPnew_GDT.lvbitx"/>
			<Item Name="ni5170caldata.dll" Type="Document" URL="ni5170caldata.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="ni5170u.dll" Type="Document" URL="ni5170u.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niHsaiGroupAConfig.dll" Type="Document" URL="/&lt;resource&gt;/niHsaiGroupAConfig.dll"/>
			<Item Name="niifwu.dll" Type="Document" URL="/&lt;resource&gt;/niifwu.dll"/>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niLVDataManip.dll" Type="Document" URL="/&lt;resource&gt;/niLVDataManip.dll"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Remove Overflow FXP(+-,14,1)x4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Remove Overflow FXP(+-,14,1)x4ch.vi"/>
			<Item Name="Remove Overflow FXP(+-,14,1)x8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Remove Overflow FXP(+-,14,1)x8ch.vi"/>
			<Item Name="Remove Overflow FXP(+-,18,1)x4ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Remove Overflow FXP(+-,18,1)x4ch.vi"/>
			<Item Name="Remove Overflow FXP(+-,18,1)x8ch.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/SubVIs/Remove Overflow FXP(+-,18,1)x8ch.vi"/>
			<Item Name="Remove Overflow.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Data Type Conversion/Remove Overflow.vi"/>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="Stream Control.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Sub VIs/Stream Control.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Track FIFO High Water.vi" Type="VI" URL="../instr.lib/NI-FPGA-VITO/FPGA/Sub VIs/Track FIFO High Water.vi"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="VITO_Scope_FPGA.lvlib" Type="Library" URL="../instr.lib/NI-FPGA-VITO/FPGA/VITO/VITO_Scope_FPGA.lvlib"/>
			<Item Name="VITO_Scope_Typedefs.lvlib" Type="Library" URL="../instr.lib/NI-FPGA-VITO/VITO_Scope_Typedefs.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="VCS App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{773E39E1-BBD6-4173-9460-6FE7C7522091}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/VCS.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{95E968AE-FBA3-4AF4-B59D-842A4FF56C3A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">VCS App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/builds/NI_AB_PROJECTNAME/App</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Packages/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_preActionVIID" Type="Ref">/My Computer/Packages/gitversioninfo/GitControl.lvlib/Pre-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6211658F-9962-4EFC-81EE-AF45E75227E4}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/VCS/App/VCS_build_log.txt</Property>
				<Property Name="Destination[0].destName" Type="Str">VCS.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/App/VCS.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/builds/NI_AB_PROJECTNAME/App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/VCS.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{FD6ECA0A-72D9-430E-91CE-13A6AC95432D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/VCS_Main.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Release_Notes.md</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Packages/DSC/CSPP_DSC.ini</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager.ini</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities.ini</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core.ini</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/SV.lib/VCS.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/VCS.ini</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">10</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">VCS App</Property>
				<Property Name="TgtF_internalName" Type="Str">VCS App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2021 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">VCS App</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">VCS.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
