Release Notes for the VCS Project
===========================================
This LabVIEW project _VCS.lvproj_ is used to develop the applications based on NI ActorFramework and CS++ libraries.

Version 0.0.0.0 16-02-2021
--------------------------
H.Brand@gsi.de, D.Neidherr@gsi.de
The VCS project was just forked. There is the master branch with some submodules, only.

Release 0.1.0.0 18-08-2021
--------------------------
H.Brand@gsi.de, D.Neidherr@gsi.de, Marcus.Jankowski@cern.ch, Jared.Croese@cern.ch

VITO Scope actor with 8 channels was successfully tested with Co-60 source.
   - external trigger PFI0
   - Gated data with timestamp
   - Pulse properties with 7 parameters
     - WCM: Window Coincicence Mask  
	 - Integral
	 - Maximum
	 - Time
	 - ToT: Time over Threshold
	 - Counter

Additional feature:
   - Delayed pulse generation, once after PFI0 trigger.

